#variable "vpc_cidr" {
#   description = "vpc cidr block"  
#}

#variable "pub_sub_cidr_block" {
#   description = "public subnet cidr block"  
#}

#variable "prv_sub_cidr_block" {
#   description = "private subnet cidr block"
#}

variable "region" {
  type        = string
  default     = "us-east-1"
  description = "default region"
}

variable "vpc_cidr" {
  type        = string
  default     = "177.0.0.0/16"
  description = "default vpc_cidr_block"
}

variable "pub_sub_cidr_block"{
   type        = string
   default     = "177.0.10.0/24"
}


variable "prv_sub_cidr_block"{
   type        = string
   default     = "177.0.20.0/24"
}

# VPC
resource "aws_vpc" "demo" { 
 cidr_block = var.vpc_cidr 
 tags = { 
          Project = "demo-assignment"
          Name = "demo" 
        }
}

# Public Subnet
resource "aws_subnet" "pub_sub" {  
 vpc_id                  = aws_vpc.demo.id  
 cidr_block              = var.pub_sub_cidr_block  
 availability_zone       = "us-east-1a" 
 map_public_ip_on_launch = true  
 tags = {    
         Project = "demo-assignment"   
         Name = "public_subnet"
        }
} 

# Private Subnet
resource "aws_subnet" "prv_sub" {  
 vpc_id                  = aws_vpc.demo.id  
 cidr_block              = var.prv_sub_cidr_block  
 availability_zone       = "us-east-1a" 
 map_public_ip_on_launch = false  
 tags = {    
         Project = "demo-assignment"   
         Name = "private_subnet"
        }
}

# Create Internet Gateway 
resource "aws_internet_gateway" "igw" {  
   vpc_id = aws_vpc.demo.id   
   tags = {    
            Project = "demo-assignment"   
            Name = "internet gateway"
          }
}

# Create Public Route Table
resource "aws_route_table" "pub_sub_rt" {
  vpc_id = aws_vpc.demo.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
   }
    tags = {
    Project = "demo-assignment"
    Name = "public subnet route table" 
 }
} 

# Create route table association of public subnet
resource "aws_route_table_association" "internet_for_pub_sub" {
  route_table_id = aws_route_table.pub_sub_rt.id
  subnet_id      = aws_subnet.pub_sub.id
}

# Create EIP for NAT GW  
resource "aws_eip" "eip_natgw" {  
     count = "1"
} 
# Create NAT gateway
resource "aws_nat_gateway" "natgateway" {  
     count         = "1"  
     allocation_id = aws_eip.eip_natgw[count.index].id  
     subnet_id     = aws_subnet.pub_sub.id
}

# Create private route table for prv sub
resource "aws_route_table" "prv_sub_rt" {
  count  = "1"
  vpc_id = aws_vpc.demo.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natgateway[count.index].id
  }
  tags = {
    Project = "demo-assignment"
    Name = "private subnet route table" 
 }
}

# Create route table association betn prv sub & NAT GW
resource "aws_route_table_association" "pri_sub_to_natgw" {
  count          = "1"
  route_table_id = aws_route_table.prv_sub_rt[count.index].id
  subnet_id      = aws_subnet.prv_sub.id
}


# Create security group for webserver
resource "aws_security_group" "webserver_sg" {
  name        = "webserver_security_group"
  description = "allow SSH access from all Source IP and HTTP from certain IP"
  vpc_id      = aws_vpc.demo.id

ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["118.189.0.0/16","116.206.0.0/16","223.25.0.0/16"]

 }

ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH"
    cidr_blocks = ["0.0.0.0/0"]
  }
egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

 tags = {
    Name = "webserver_sg"
    Project = "demo-assignment"
  }
}

# Create security group for load balancer
resource "aws_security_group" "lb_sg" {
  name        = "load_balancer_sg"
  description = "listen to TCP port of SSH and HTTP"
  vpc_id      = aws_vpc.demo.id
ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH"
    cidr_blocks = ["0.0.0.0/0"]
  }
egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
 
 tags = {
    Name = "SG for load balancer"
    Project = "demo-assignment" 
  } 
}

#Create Ec2 instance
resource "aws_instance" "ec2-demo" {
    ami = "ami-0aa2b7722dc1b5612"
    subnet_id = aws_subnet.prv_sub.id
    instance_type = "t2.micro"
    key_name = "demo"
    security_groups = [aws_security_group.webserver_sg.id]
    
    tags = {
      Name = "web-01"
    }
}

#create target_group
resource "aws_lb_target_group" "demo-tg" {
   name               = "demo-target-group"
   target_type        = "instance"
   port               = 80
   protocol           = "TCP"
   vpc_id             = aws_vpc.demo.id
   health_check {
      healthy_threshold   = "3"
      interval            = "20"
      unhealthy_threshold = "2"
      timeout             = "10"
      path                = "/"
      port                = 80
  }
} 

# Attach the target group for "test" ALB
resource "aws_lb_target_group_attachment" "tg_attach" {
    target_group_arn = aws_lb_target_group.demo-tg.arn
    target_id        = "${aws_instance.ec2-demo.id}"
    port             = 80
}

# Create ALB
resource "aws_lb" "demo-lb" {
  name              = "Demo-App-LB"
  internal           = false
  load_balancer_type = "network"
  subnets          = [aws_subnet.pub_sub.id]       
  tags = {
        name  = "Demo-AppLoadBalancer"
        Project = "demo-assignment"
       }
}


# Create ALB Listener for TCP:22 & TCP:80
resource "aws_lb_listener" "demo-listener-80" {
  load_balancer_arn = aws_lb.demo-lb.arn
  port              = "80"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.demo-tg.id
  }
}

resource "aws_lb_listener" "demo-listener-22" {
  load_balancer_arn = aws_lb.demo-lb.arn
  port              = "22"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.demo-tg.id
  }
}

#Create an IAM user with read only permission
resource "aws_iam_user" "user" {
  name = "demo"
}

data "aws_iam_policy" "readonly" {
  arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}
# Grant the read only permission to all AWS resources.
resource "aws_iam_user_policy_attachment" "attach-user" {
  user       = "${aws_iam_user.user.name}"
  policy_arn = "${data.aws_iam_policy.readonly.arn}"
}

   




